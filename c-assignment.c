#include <stdio.h>
#include <math.h>

int main()
{
    double a, b, c, discriminant, root1, root2;

    printf("Enter coefficients (a, b, c) of the quadratic equation:");
    scanf("%lf%lf%lf",&a,&b,&c);
    if(a!=0){

    discriminant = b * b - 4 * a * c;}
    else{
            printf("Invalid input");
    }

    if (discriminant > 0 ){

        root1 = (-b +
                 sqrt(discriminant))/(2*a);
        root2 = (-b -
                 sqrt(discriminant))/(2*a);
                 printf("Roots are real and distinct:%.2lf and %.2lf\n",root1,root2);
    }else if(discriminant==0){
        root1 = root2 = -b/(2*a);
        printf("Roots are real and equal:%.2lf\n",root1);
    }else{
        double realPart = -b / (2*a);
        double imagineryPart =
    sqrt(-discriminant) / (2 * a);
        printf("Roots are complex:%.2lf + %.2lf and %.2lf - %.2lf\n",realPart,imagineryPart,realPart,imagineryPart);
    }
    return 0;


}
